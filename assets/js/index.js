document.querySelectorAll('iframe:not(.embed-responsive-item), video:not(.embed-responsive-item)')
  .forEach(element => {
    let parent = element.parentNode;
    let wrapper = document.createElement('div');

    // set the wrapper as child (instead of the element)
    parent.replaceChild(wrapper, element);

    // set element as child of wrapper
    wrapper.appendChild(element);

    wrapper.classList.add('embed-responsive', 'embed-responsive-16by9', 'mx-auto');
    element.classList.add('embed-responsive-item');
  });

const navPosts = (delta) => {
  const posts = document.querySelectorAll('div.single-post');

  if (delta > 0) {
    let i = 0;
    for (; i < posts.length; i++) {
      let topPos = posts[i].getBoundingClientRect().top;
      if (Math.abs(topPos) < 1) topPos = 0;
      if (topPos > 0) {
        posts[i].scrollIntoView({behavior: 'smooth'});
        return;
      }
    }
    if (i >= posts.length)
      posts[posts.length-1].scrollIntoView({behavior: 'smooth', block: "end"});
  } else {
    for (let i = posts.length - 1; i >= 0; i--) {
      let topPos = posts[i].getBoundingClientRect().top;
      if (Math.abs(topPos) < 1) topPos = 0;
      if (topPos < 0) {
        posts[i].scrollIntoView({behavior: 'smooth'});
        return;
      }
    }
  }
}
